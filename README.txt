--- this module ---

module-info.java:
    module EscapingReferences {
        exports com.antoonvereecken.escapingreferences.customers;
    }



--- new module/project ---

module-info.java:
    module EscapingReferencesClient {
        requires EscapingReferences;
    }


src:
    package com.antoonvereecken.escapingreferences.client;

    import com.antoonvereecken.escapingreferences.customers.CustomerRecords;
    import com.antoonvereecken.escapingreferences.customers.ReadOnlyCustomer;

    public class Main {
        public static void main(String[] args) {

            CustomerRecords records = new CustomerRecords();
            records.addCustomer("John");
            records.addCustomer("Simon");

            for (ReadOnlyCustomer c : records) {
                System.out.println(c);
            }
        }
    }