package com.antoonvereecken.escapingreferences.customers;

import com.antoonvereecken.escapingreferences.customers.customerimplementation.Customer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CustomerRecords implements Iterable<Customer> {

	private Map<String, Customer> records;
	
	public CustomerRecords() { this.records = new HashMap<>(); }
	public void addCustomer(String name) {
		this.records.put(name, new Customer(name));
	}

	public Map<String, Customer> getCustomers() {
		/** Java 8: creates another map & copies the references to that map */
		//	return Collections.unmodifiableMap(this.records);
		/** Java 10+: this doesn't actually create a new map -> performance gains */
		return Map.copyOf(this.records);
	}

	@Override
	public Iterator<Customer> iterator() {
		return records.values().iterator();
	}

	public ReadOnlyCustomer find(String name){
		return new Customer(records.get(name));
	}


}
