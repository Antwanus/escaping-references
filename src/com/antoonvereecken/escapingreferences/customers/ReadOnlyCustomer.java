package com.antoonvereecken.escapingreferences.customers;

public interface ReadOnlyCustomer {
    String getName();
    String toString();


}
